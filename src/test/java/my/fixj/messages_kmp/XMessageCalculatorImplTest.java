package my.fixj.messages_kmp;

import org.junit.Test;

public class XMessageCalculatorImplTest {

    @Test
    public void testBid() {
        XMessageBuilder b = new XMessageBuilder();

        //depth = 2
        b.create()
                .newEnt(1, 10, 10)
                .newEnt(2, 8, 8)
                .newEnt(3, 6, 6)
                .newEnt(4, 4, 4)
                .calculate().test(1, 2).isnew(1, 2).actsize(2);

        b.create()
                .newEnt(5, 12, 12)
                .newEnt(6, 11, 11)
                .newEnt(7, 6, 6)
                .newEnt(8, 4, 4)
                .calculate().test(5, 6).isnew(5, 6).actsize(2);

        b.create()
                .newEnt(9, 10, 10)
                .newEnt(10, 9, 9)
                .newEnt(11, 6, 6)
                .newEnt(12, 4, 4)
                .calculate().test(5, 6).actsize(0);

        b.create()
                .newEnt(13, 10, 10)
                .newEnt(14, 9, 9)
                .newEnt(15, 6, 6)
                .newEnt(16, 4, 4)
                .delEnt(5).delEnt(6)
                .calculate().test(13, 14)
                .isdel(5, 6).isnew(13, 14).actsize(4)

                .create()
                .newEnt(17, 10, 10)
                .newEnt(18, 9, 9)
                .newEnt(19, 6, 6)
                .newEnt(20, 4, 4)
                .calculate().test(17, 18).isupdate(17, 18).actsize(2)

                .create()
                .newEnt(21, 5, 5)
                .newEnt(22, 3, 3)
                .newEnt(23, 2, 2)
                .newEnt(24, 1, 1)
                .delEnt(17)
                .delEnt(18)
                .calculate().test(21, 22).isnew(21, 22).isdel(17, 18).actsize(4)

                .create()
                .newEnt(25, 5, 5)
                .newEnt(26, 3, 3)
                .newEnt(27, 2, 2)
                .newEnt(28, 1, 1)
                .delEnt(21)
                .delEnt(22)
                .calculate().test(25, 26).isnew(25, 26).isdel(21, 22).actsize(4);
    }
}