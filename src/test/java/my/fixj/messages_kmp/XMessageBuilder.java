package my.fixj.messages_kmp;

import my.fixj.messages.XMessage;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

class XMessageBuilder {
    private XMessage message;
    private Map<String, XMessageCalculatorImpl.PrintXEntry> exp;
    private XMessageCalculatorImpl.OrderBookSnapshot bk;
    private XMessageCalculatorImpl calculator = new XMessageCalculatorImpl();

    XMessageBuilder newEnt(int id, int price, int size) {
        XMessage.XEntry x = message.addXEntry();
        x.setMDEntryID(Integer.toString(id));
        x.setMDEntryType(XMessage.MDEntryType.BID);
        x.setMDUpdateAction(0);
        x.setMDEntryPx(new BigDecimal(price));
        x.setMDEntrySize(new BigDecimal(size));
        return this;
    }

    XMessageBuilder delEnt(int id) {
        XMessage.XEntry x = message.addXEntry();
        x.setMDEntryID(Integer.toString(id));
        x.setMDEntryType(XMessage.MDEntryType.BID);
        x.setMDUpdateAction(2);
        return this;
    }

    XMessageBuilder create() {
        message = new XMessage();
        return this;
    }

    XMessage get() {
        return message;
    }

    Map<String, XMessageCalculatorImpl.PrintXEntry> toMap(Collection<XMessageCalculatorImpl.PrintXEntry> ents) {
        return ents.stream().collect(Collectors.toMap(t -> t.getId(), v -> v));
    }

    XMessageBuilder calculate() {
        bk = calculator.calculate(get(), 2);
        exp = toMap(bk.getBids());
        return this;
    }

    XMessageBuilder test(int... ids) {
        for (int id : ids) {
            Assert.assertTrue(exp.containsKey(Integer.toString(id)));
        }
        Assert.assertTrue(exp.size() == 2);
        return this;
    }

    XMessageBuilder isupdate(int... ids) {
        Map<String, XMessageCalculatorImpl.PrintXEntry> ups = bk.getActions().stream().collect(
                Collectors.toMap(t -> t.getId(), v -> v));
        for (int id : ids) {
            Assert.assertTrue(ups.containsKey(Integer.toString(id)));
            Assert.assertTrue(ups.get(Integer.toString(id)).getState() == XMessageCalculatorImpl.State.Update);
        }
        return this;
    }

    XMessageBuilder isnew(int... ids) {
        Map<String, XMessageCalculatorImpl.PrintXEntry> ups = bk.getActions().stream().collect(
                Collectors.toMap(t -> t.getId(), v -> v));
        for (int id : ids) {
            Assert.assertTrue(ups.containsKey(Integer.toString(id)));
            Assert.assertTrue(ups.get(Integer.toString(id)).getState() == XMessageCalculatorImpl.State.New);
        }
        return this;
    }

    XMessageBuilder isdel(int... ids) {
        Map<String, XMessageCalculatorImpl.PrintXEntry> ups = bk.getActions().stream().collect(
                Collectors.toMap(t -> t.getId(), v -> v));
        for (int id : ids) {
            Assert.assertTrue(ups.containsKey(Integer.toString(id)));
            Assert.assertTrue(ups.get(Integer.toString(id)).getState() == XMessageCalculatorImpl.State.Delete);
        }
        return this;
    }

    XMessageBuilder actsize(int sz) {
        Assert.assertEquals(bk.getActions().size(), sz);
        return this;
    }
}