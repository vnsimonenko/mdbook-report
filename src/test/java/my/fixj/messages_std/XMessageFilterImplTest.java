package my.fixj.messages_std;

import my.fixj.messages.XMessageFilter;
import my.fixj.messages_kmp.XMessageDateTime;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.regex.Pattern;

public class XMessageFilterImplTest {

    @Test
    public void filter() throws IOException {
        final Pattern pattern = Pattern.compile("[|]34=13[012345][|]");
        final int numberOfMessages[] = {0};
        XMessageDateTime sendingDT = new XMessageDateTime();
        sendingDT.setYear(2015);
        sendingDT.setMonth(3);
        sendingDT.setDay(3);
        XMessageFilterImpl ft = new XMessageFilterImpl('|');
        Assert.assertTrue(
                ft.filter(getClass().getResource("/messages.log").getFile(), "EUR/USD", sendingDT, new XMessageFilter.MessageTextHandler() {
                    @Override
                    public void handle(String message) {
                        if (message != null && pattern.matcher(message).find()) {
                            numberOfMessages[0]++;
                        }
                    }

                    @Override
                    public void readBytes(double progress) {
                    }
                })
        );

        Assert.assertEquals(6, numberOfMessages[0]);
    }
}