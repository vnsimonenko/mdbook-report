package my.fixj.messages_std;

import my.fixj.messages.XMessageProcessor;
import my.fixj.messages_kmp.XMessageDateTime;
import my.fixj.reports.ReportBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.Silent.class)
public class XMessageProcessorImplTest {

    @Mock
    private XMessageProcessor.ProgressIndicator indicator;

    @Test
    public void process() throws Exception {
        doNothing().when(indicator).readBytes(anyDouble());
        doNothing().when(indicator).readMessages(anyInt(),anyInt());


        XMessageFilterImpl filter = new XMessageFilterImpl('|');

        String inputMessagesFileName = getClass().getResource("/messages.log").getFile();
        String symbolName = "EUR/USD";
        int depth = 2;
        XMessageDateTime sendingDT = new XMessageDateTime();
        sendingDT.setYear(2015);
        sendingDT.setMonth(3);
        sendingDT.setDay(3);
        String reportFileName = "report.csv";
        ReportBuilder.ReportFormat reportFormat = ReportBuilder.ReportFormat.CSV;
        XMessageProcessorImpl processor = new XMessageProcessorImpl();
        processor.setFilter(filter);
        int numberOfMessages = processor.process(inputMessagesFileName, symbolName, sendingDT, depth, reportFileName, reportFormat, indicator);
        Assert.assertEquals(6, numberOfMessages);
    }
}