<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="date">2015-01-06T17:51:01.67+05:30</xsl:param>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
                <link rel="stylesheet"
                      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"/>
                <style>

                </style>
            </head>
            <h3>Market Data Incremental Refresh</h3>
            <table style="width:100%" class="table table-bordered table-condensed">
                <tbody>
                    <tr>
                        <td>Log file</td>
                        <td>
                            <xsl:value-of select="messages/log_name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Symbol Name</td>
                        <td>
                            <xsl:value-of select="messages/symbol_name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Book depth</td>
                        <td>
                            <xsl:value-of select="messages/book_depth"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Start Time</td>
                        <td>
                            <xsl:value-of
                                    select="format-dateTime(messages/start, '[Y0001][M01][D01]-[H01]:[m01]:[s01].[f001]')"/>
                        </td>
                    </tr>
                    <tr>
                        <td>End Time</td>
                        <td>
                            <xsl:value-of
                                    select="format-dateTime(messages/finish, '[Y0001][M01][D01]-[H01]:[m01]:[s01].[f001]')"/>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered table-condensed">
                <tbody>
                    <tr valign="top" width="100%">
                        <td width="100%">
                            <xsl:for-each select="messages/messages/message">
                                <table width="100%" class="table table-bordered table-condensed">
                                    <tbody>
                                        <tr>
                                            <td width="100%" class="table table-bordered table-condensed">
                                                <table width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td width="50%">
                                                                <h1>
                                                                    <xsl:value-of select="msgSeqNum"/>
                                                                </h1>
                                                            </td>
                                                            <td width="50%">
                                                                <table class="table table-bordered table-condensed">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Sending Time</td>
                                                                            <td>
                                                                                <xsl:value-of
                                                                                        select="format-dateTime(sendingDate, '[Y0001][M01][D01]-[H01]:[m01]:[s01].[f001]')"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Receiving Time</td>
                                                                            <td>
                                                                                <xsl:value-of
                                                                                        select="format-dateTime(receiveDate, '[Y0001][M01][D01]-[H01]:[m01]:[s01].[f001]')"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Difference(ms)</td>
                                                                            <td>
                                                                                <xsl:value-of select="difference"/>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" class="table table-bordered table-condensed">
                                                    <tbody>
                                                        <tr valign="top">
                                                            <td width="33%">
                                                                <table width="100%" border="1">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="30%">ID</th>
                                                                            <th width="10%">Action</th>
                                                                            <th width="10%">Side</th>
                                                                            <th width="25%">Price</th>
                                                                            <th width="25%">Size</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <xsl:for-each select="entries/entry">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <xsl:value-of select="id"/>
                                                                                </td>
                                                                                <td>
                                                                                    <xsl:value-of select="action"/>
                                                                                </td>
                                                                                <td align="center">
                                                                                    <xsl:value-of select="side"/>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <xsl:if test="price!=0">
                                                                                        <xsl:value-of
                                                                                                select="format-number(price, '#,#####0.00000')"/>
                                                                                    </xsl:if>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <xsl:value-of select="size"/>
                                                                                </td>
                                                                            </tr>
                                                                        </xsl:for-each>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="33%">
                                                                <table width="100%" border="1">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th width="50%" colspan="2">BID</th>
                                                                            <th width="50%" colspan="2">ASK</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="25%">Price</th>
                                                                            <th width="25%">Size</th>
                                                                            <th width="25%">Price</th>
                                                                            <th width="25%">Size</th>
                                                                        </tr>
                                                                        <xsl:for-each select="bidBooks/bidBook">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <xsl:value-of
                                                                                            select="format-number(price, '#,#####0.00000')"/>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <xsl:value-of select="size"/>
                                                                                </td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </xsl:for-each>
                                                                        <xsl:for-each select="askBooks/askBook">
                                                                            <tr>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td align="right">
                                                                                    <xsl:value-of
                                                                                            select="format-number(price, '#,#####0.00000')"/>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <xsl:value-of select="size"/>
                                                                                </td>
                                                                            </tr>
                                                                        </xsl:for-each>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="33%">
                                                                <table width="100%" border="1">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th width="20%">Action</th>
                                                                            <th width="20%">Side</th>
                                                                            <th width="30%">Price</th>
                                                                            <th width="30%">Size</th>
                                                                        </tr>
                                                                        <xsl:for-each
                                                                                select="actOrderBooks/actOrderBook">
                                                                            <tr>
                                                                                <td>
                                                                                    <xsl:value-of select="action"/>
                                                                                </td>
                                                                                <td>
                                                                                    <xsl:value-of select="side"/>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <xsl:value-of
                                                                                            select="format-number(price, '#,#####0.00000')"/>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <xsl:value-of select="size"/>
                                                                                </td>
                                                                            </tr>
                                                                        </xsl:for-each>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </xsl:for-each>
                        </td>
                    </tr>
                </tbody>
            </table>
        </html>
    </xsl:template>
</xsl:stylesheet>