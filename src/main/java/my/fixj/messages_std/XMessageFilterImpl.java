package my.fixj.messages_std;

import my.fixj.messages.XMessageFilter;
import my.fixj.messages.XMessageParser;
import my.fixj.messages_kmp.XMessageDateTime;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMessageFilterImpl implements XMessageFilter {
    private char splitter;
    private XMessageParserImpl parser;

    public XMessageFilterImpl(char splitter) {
        this.splitter = splitter;
    }

    @Override
    public boolean filter(String fileName, String symbolName, XMessageDateTime sendingDate, MessageTextHandler handler) throws IOException {
        boolean found = false;
        String sp = "[" + splitter + "]";
        String exp = String.format("(: 8=FIX.4.4%1$s[0-9]+=[0-9]+%1$s35=X%1s).*%1$s52=([0-9:.-]+)%1$s.*%1$s55=%2$s+%1$s.*$", sp, symbolName);
        String expectedSendingDate = sendingDate.toString();
        try (BufferedReader reader = createReader(fileName, handler)) {
            Pattern pt = Pattern.compile(exp);
            String ln;
            while ((ln = reader.readLine()) != null) {
                Matcher mt = pt.matcher(ln);
                if (mt.find() && (expectedSendingDate == null || mt.group(2).startsWith(expectedSendingDate))) {
                    handler.handle(ln);
                    if (!found)
                        found = true;
                }
            }
        }
        if (found)
            handler.handle(null);
        return found;
    }

    @Override
    public XMessageParser getParser() {
        if (parser == null)
            parser = new XMessageParserImpl(splitter);
        return parser;
    }

    private BufferedReader createReader(String fileName, MessageTextHandler handler) throws IOException {
        long allBytes = getAllBytesInFile(fileName);
        return new BufferedReader(new FileReader(fileName), 65536) {
            double readBytes;
            int period = 0;

            @Override
            public String readLine() throws IOException {
                String ln = super.readLine();
                if (period > 10000) {
                    handler.readBytes(readBytes / allBytes);
                    period = 0;
                }
                readBytes += ln == null ? 0 : ln.length();
                period++;
                return ln;
            }
        };
    }

    private long getAllBytesInFile(String fileName) throws IOException {
        Map<String, Object> atts = Files.readAttributes(Paths.get(fileName), "size");
        return (Long) atts.get("size");
    }
}
