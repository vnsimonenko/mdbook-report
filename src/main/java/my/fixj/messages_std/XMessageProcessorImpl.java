package my.fixj.messages_std;

import my.fixj.messages.XMessage;
import my.fixj.messages.XMessageFilter;
import my.fixj.messages.XMessageParser;
import my.fixj.messages.XMessageProcessor;
import my.fixj.messages_kmp.XMessageDateTime;
import my.fixj.reports.CsvReportBuilder;
import my.fixj.reports.HtmlReportBuilder;
import my.fixj.reports.ReportBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class XMessageProcessorImpl implements XMessageProcessor {
    private XMessageFilter filter;
    private LinkedBlockingQueue<LinkedItem> inqueue = new LinkedBlockingQueue<>();
    private LinkedItem head;
    private final Object notifyReport = new Object();
    private XMessageParser parser;
    private AtomicInteger numberOfLoadMessages;
    private final static Logger LOGGER = LoggerFactory.getLogger(XMessageProcessorImpl.class);
    private volatile boolean isTerminatedProcess;
    private boolean isFJP = true; //algorithm type is ForkJoinTask (ForkJoinPool)

    public XMessageProcessorImpl() {
    }

    public XMessageProcessorImpl(boolean fjp) {
        this();
        isFJP = fjp;
    }

    public void setFilter(XMessageFilter filter) {
        this.filter = filter;
        this.parser = filter.getParser();
    }

    @Override
    public synchronized int process(String inputMessagesFileName, String symbolName, XMessageDateTime sending, int depth, String reportFileName, ReportBuilder.ReportFormat reportFormat, ProgressIndicator indicator) throws Exception {
        long startProcessDateTime = System.currentTimeMillis();
        ReportBuilder reportBuilder;
        switch (reportFormat) {
            case HTML:
                reportBuilder = new HtmlReportBuilder(reportFileName,
                        inputMessagesFileName, symbolName, depth);
                break;
            case CSV:
            default:
                reportBuilder = new CsvReportBuilder(reportFileName,
                        inputMessagesFileName, symbolName, depth);
        }

        if (filter == null) {
            filter = new XMessageFilterImpl((char) 1);
            parser = filter.getParser();
        }

        numberOfLoadMessages = new AtomicInteger(0);
        int numberOfReportMessages = 0;
        try {
            ExecutorService service = Executors.newSingleThreadExecutor();
            Future<Integer> reportFuture = service.submit(new MessageReportRunnable(reportBuilder, indicator));
            if (isFJP) {
                ForkJoinPool forkJoinPool = new ForkJoinPool();
                forkJoinPool.execute(new ForkJoinPoolMessageParseTask());
            } else {
                ExecutorService parserService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() - 1);
                for (int i = 0; i < Runtime.getRuntime().availableProcessors() - 1; i++)
                    parserService.submit(new MessageParseTask());
            }
            if (!filter.filter(inputMessagesFileName, symbolName, sending, new XMessageTextHandler(indicator))) {
                return 0;
            }
            numberOfReportMessages = reportFuture.get();
        } catch (IOException ex) {
            isTerminatedProcess = true;
        }
        long endProcessDateTime = System.currentTimeMillis();
        printInfo(numberOfLoadMessages.get(), numberOfReportMessages,
                inputMessagesFileName, symbolName, sending, depth, reportFileName, reportFormat,
                startProcessDateTime, endProcessDateTime);

        return numberOfReportMessages;
    }

    @Override
    public synchronized void setTerminatedProcess(boolean isTerminatedProcess) {
        this.isTerminatedProcess = isTerminatedProcess;
    }

    private boolean isTerminatedProcess() {
        return isTerminatedProcess || Thread.currentThread().isInterrupted();
    }

    private void printInfo(int numberOfLoadMessagesProcessed,
                           int numberOfReportMessagesProcessed,
                           String inputMessagesFileName,
                           String symbolName,
                           XMessageDateTime sending,
                           int depth,
                           String reportFileName,
                           ReportBuilder.ReportFormat reportFormat,
                           long startDateTime,
                           long endDateTime) {
        String info = "\n********** INFO **********"
                + "\ninput file name: " + inputMessagesFileName
                + "\nreport file name: " + reportFileName
                + "\nsymbol name: " + symbolName
                + "\nsending: " + sending
                + "\ndepth: " + depth
                + "\nreport format: " + reportFormat
                + "\nnumberOfParsedMessagesProcessed: " + numberOfLoadMessagesProcessed
                + "\nnumberOfReportMessagesProcessed: " + numberOfReportMessagesProcessed
                + "\nstart: " + new Date(startDateTime) + ", finish: " + new Date(endDateTime) + ", diff[ms]: " + (endDateTime - startDateTime)
                + "\nstatus: " + (numberOfLoadMessagesProcessed == numberOfReportMessagesProcessed ? "success" : "fail")
                + "\n**********";
        LOGGER.info(info);
    }

    private class XMessageTextHandler implements XMessageFilter.MessageTextHandler {
        private ProgressIndicator indicator;
        private LinkedItem li;

        XMessageTextHandler(ProgressIndicator indicator) {
            this.indicator = indicator;
        }

        @Override
        public void handle(String message) {
            try {
                if (message == null) {
                    li.next = new LinkedItem(null);
                    inqueue.put(li.next);
                } else {
                    if (li == null) {
                        li = new LinkedItem(message);
                        head = li;
                    } else {
                        li.next = new LinkedItem(message);
                        li = li.next;
                    }
                    inqueue.put(li);
                    numberOfLoadMessages.incrementAndGet();
                }
            } catch (InterruptedException e) {
                isTerminatedProcess = true;
            }
        }

        @Override
        public void readBytes(double progress) {
            indicator.readBytes(progress);
        }
    }

    private class ForkJoinPoolMessageParseTask extends RecursiveAction {

        ForkJoinPoolMessageParseTask() {
            super();
        }

        @Override
        protected void compute() {
            try {
                if (isTerminatedProcess())
                    return;
                LinkedItem li = inqueue.take();
                if (li.raw != null) {
                    ForkJoinPoolMessageParseTask task = new ForkJoinPoolMessageParseTask();
                    task.fork();
                    li.xMessage = parser.parse(li.raw);
                    li.raw = null;
                    synchronized (notifyReport) {
                        notifyReport.notifyAll();
                    }
                    //task.join(); don't use
                } else {
                    synchronized (notifyReport) {
                        notifyReport.notifyAll();
                    }
                }
            } catch (InterruptedException | ParseException e) {
                isTerminatedProcess = true;
            }
        }
    }

    private class MessageParseTask implements Runnable {
        @Override
        public void run() {
            try {
                while (!isTerminatedProcess()) {
                    LinkedItem li = inqueue.take();
                    if (li.raw != null) {
                        li.xMessage = parser.parse(li.raw);
                        li.raw = null;
                        synchronized (notifyReport) {
                            notifyReport.notifyAll();
                        }
                    } else {
                        synchronized (notifyReport) {
                            notifyReport.notifyAll();
                        }
                        break;
                    }
                }
            } catch (InterruptedException | ParseException e) {
                isTerminatedProcess = true;
            }
        }
    }

    private class MessageReportRunnable implements Callable<Integer> {
        int nXMessage = 0;
        ReportBuilder reportBuilder;
        ProgressIndicator progressIndicator;

        MessageReportRunnable(ReportBuilder reportBuilder, ProgressIndicator progressIndicator) {
            this.reportBuilder = reportBuilder;
            this.progressIndicator = progressIndicator;
        }

        public Integer call() throws Exception {
            try {
                synchronized (notifyReport) {
                    while (!isTerminatedProcess() && (head == null || head.xMessage == null))
                        notifyReport.wait();
                }
                LinkedItem li = head;
                reportBuilder.begin();
                reportBuilder.add(li.xMessage);
                nXMessage++;
                if (progressIndicator != null)
                    progressIndicator.readMessages(nXMessage + 1, li.xMessage.getMsgSeqNum());
                while (!isTerminatedProcess()) {
                    if (li.next != null) {
                        if (li.next.raw == null && li.next.xMessage == null)
                            break;
                        if (li.next.xMessage == null) {
                            synchronized (notifyReport) {
                                notifyReport.wait(100);
                            }
                            continue;
                        }
                        li = li.next;
                    } else {
                        synchronized (notifyReport) {
                            notifyReport.wait(100);
                        }
                        continue;
                    }
                    reportBuilder.add(li.xMessage);
                    if (nXMessage == 1)
                        head = null; //gc clear linked list of xmessages
                    nXMessage++;
                    if (progressIndicator != null)
                        progressIndicator.readMessages(nXMessage, li.xMessage.getMsgSeqNum());
                }
                reportBuilder.commit();
            } catch (InterruptedException ex) {
                isTerminatedProcess = true;
            }
            return nXMessage;
        }
    }

    private static class LinkedItem {
        private String raw;
        private XMessage xMessage;
        private LinkedItem next;

        LinkedItem(String raw) {
            this.raw = raw;
        }
    }
}
