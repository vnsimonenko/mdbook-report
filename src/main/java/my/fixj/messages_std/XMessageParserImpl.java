package my.fixj.messages_std;

import my.fixj.messages.XMessage;
import my.fixj.messages.XMessageParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class XMessageParserImpl implements XMessageParser {
    private final static Logger LOGGER = LoggerFactory.getLogger(XMessageParserImpl.class);
    private char splitter;

    XMessageParserImpl(char splitter) {
        this.splitter = splitter;
    }

    @Override
    public XMessage parse(String message) throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
        int i = message.indexOf(":I:N ") + 5;
        String receiveDateTime = message.substring(i, i + 21);
        String exp = String.format("%1$s*([0-9]+)=([0-9a-zA-Z\\-:./]+)%1$s+", "[" + splitter + "]");
        Matcher matcher = Pattern.compile(exp).matcher(message);
        XMessage xMessage = new XMessage();
        setField("receiveDateTime", receiveDateTime, xMessage, dateFormat);
        while (matcher.find()) {
            setField(matcher.group(1), matcher.group(2), xMessage, dateFormat);
        }
        return xMessage;
    }

    private void setField(String key, String value, XMessage xMessage, SimpleDateFormat dateFormat) throws ParseException {
        try {
            switch (key) {
                case "34":
                    xMessage.setMsgSeqNum(Integer.parseInt(value));
                    break;
                case "52":
                    xMessage.setSendingTime(dateFormat.parse(value));
                    break;
                case "279":
                    XMessage.XEntry xEntry = xMessage.addXEntry();
                    xEntry.setMDUpdateAction(Integer.parseInt(value));
                    break;
                case "269":
                    xMessage.getLastXEntry().setMDEntryType(XMessage.MDEntryType.values()[Integer.parseInt(value)]);
                    break;
                case "278":
                    xMessage.getLastXEntry().setMDEntryID(value);
                    break;
                case "55":
                    xMessage.getLastXEntry().setSymbol(value);
                    break;
                case "270":
                    xMessage.getLastXEntry().setMDEntryPx(new BigDecimal(value));
                    break;
                case "271":
                    xMessage.getLastXEntry().setMDEntrySize(new BigDecimal(value));
                    break;
                case "receiveDateTime":
                    xMessage.setReceiveDateTime(dateFormat.parse(value));
                    break;
            }
        } catch (ParseException | NumberFormatException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }
    }
}
