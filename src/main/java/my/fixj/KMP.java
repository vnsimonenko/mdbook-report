package my.fixj;

import java.io.IOException;
import java.io.Reader;

/**
 * Knuth-Morris-Pratt substring search.
 * Reads in two strings, the pattern and the input text, and
 * searches for the pattern in the input text using the
 * KMP algorithm.
 *
 * @see "https://algs4.cs.princeton.edu/53substring/KMP.java.html"
 **/
public class KMP {
    private final int R;       // the radix
    private int[][] dfa;       // the KMP automoton

    private char[] pattern;    // either the character array for the pattern
    private String pat;        // or the pattern string

    /**
     * Preprocesses the pattern string.
     *
     * @param pattern the pattern string
     * @param R       the alphabet size
     */
    public KMP(char[] pattern, int R) {
        this.R = R;
        this.pattern = new char[pattern.length];
        for (int j = 0; j < pattern.length; j++)
            this.pattern[j] = pattern[j];

        // build DFA from pattern
        int m = pattern.length;
        dfa = new int[R][m];
        dfa[pattern[0]][0] = 1;
        for (int x = 0, j = 1; j < m; j++) {
            for (int c = 0; c < R; c++)
                dfa[c][j] = dfa[c][x];     // Copy mismatch cases. 
            dfa[pattern[j]][j] = j + 1;    // Set match case. 
            x = dfa[pattern[j]][x];        // Update restart state. 
        }
    }

    /**
     * Returns the index of the first occurrrence of the pattern string
     * in the text string.
     *
     * @param txt the text string
     * @return the index of the first occurrence of the pattern string
     * in the text string; N if no such match
     */
    public int search(String txt) {

        // simulate operation of DFA on text
        int m = pat.length();
        int n = txt.length();
        int i, j;
        for (i = 0, j = 0; i < n && j < m; i++) {
            j = dfa[txt.charAt(i)][j];
        }
        if (j == m)
            return i - m;    // found
        return n;            // not found
    }

    /**
     * Returns the index of the first occurrrence of the pattern string
     * in the text string.
     *
     * @param text the text string
     * @return the index of the first occurrence of the pattern string
     * in the text string; N if no such match
     */
    public int search(char[] text) {

        // simulate operation of DFA on text
        int m = pattern.length;
        int n = text.length;
        int i, j;
        for (i = 0, j = 0; i < n && j < m; i++) {
            j = dfa[text[i]][j];
        }
        if (j == m)
            return i - m;    // found
        return n;                    // not found
    }

    public int searchAndFill(Reader reader, StringBuilder sb) throws IOException {
        return searchAndFill(reader, sb, -1);
    }

    public int searchAndFill(Reader reader, StringBuilder sb, int prefix) throws IOException {
        int m = pattern.length;
        int i = 0;
        int j = 0;
        int ch;
        if (prefix != -1 && j < m) {
            j = dfa[prefix][j];
            if (sb != null)
                sb.append((char) prefix);
            i++;
            if (j == m)
                return i - m; // found
        }
        while (j < m && (ch = reader.read()) != -1) {
            j = dfa[ch][j];
            if (sb != null)
                sb.append((char) ch);
            i++;
        }
        if (j == m) {
            return i - m; // found
        }
        return -1;
    }
}