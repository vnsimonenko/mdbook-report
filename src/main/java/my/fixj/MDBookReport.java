package my.fixj;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import my.fixj.messages.XMessageFilter;
import my.fixj.messages.XMessageProcessor;
import my.fixj.messages_kmp.XMessageDateTime;
import my.fixj.reports.ReportBuilder;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MDBookReport extends Application {
    private Thread printThread;
    private static Logger LOGGER;

    @Override
    public void stop() {
        System.exit(0);
    }

    @Override
    public void start(final Stage stage) {
        LOGGER.debug("Start");
        createUi(stage);
        stage.setTitle("Support Tools: Book Printer");
        stage.show();
    }

    private void createUi(Stage primaryStage) {
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 700, 400, Color.WHITE);
        primaryStage.setMaxWidth(700);
        primaryStage.setMaxHeight(400);
        primaryStage.setMinWidth(700);
        primaryStage.setMinHeight(400);
        primaryStage.setResizable(false);


        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(10));
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        ColumnConstraints column1 = new ColumnConstraints(200);
        ColumnConstraints column2 = new ColumnConstraints(300, 500, 700);
        ColumnConstraints column3 = new ColumnConstraints(80, 80, 80);
        column2.setHgrow(Priority.ALWAYS);
        gridpane.getColumnConstraints().addAll(column1, column2, column3);

        //****************************************************

        Label fixlogFileLbl = new Label("Fix Log File *");
        final TextField fixLogFileFld = new TextField();
        fixLogFileFld.setText(ApplicationProperties.KEY.InFile.asString(""));

        Button inFileButt = new Button("Browse");
        inFileButt.setFocusTraversable(false);
        inFileButt.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            Path path = Paths.get(StringUtils.defaultIfBlank(fixLogFileFld.getText(), ""));
            if (path.toFile().exists())
                fileChooser.setInitialDirectory(Files.isDirectory(path) ? path.toFile() : path.getParent().toFile());
            File f = fileChooser.showOpenDialog(primaryStage);
            if (f != null)
                fixLogFileFld.setText(f.getAbsolutePath());
        });

        GridPane.setHalignment(fixlogFileLbl, HPos.LEFT);
        gridpane.add(fixlogFileLbl, 0, 0);

        GridPane.setHalignment(fixLogFileFld, HPos.LEFT);
        gridpane.add(fixLogFileFld, 1, 0);

        GridPane.setHalignment(inFileButt, HPos.RIGHT);
        gridpane.add(inFileButt, 2, 0);

        //****************************************************

        Label reportLbl = new Label("Report File *");
        final TextField reportFld = new TextField();
        reportFld.setText(ApplicationProperties.KEY.OutFile.asString(""));

        Button outFileButt = new Button("Browse");
        outFileButt.setFocusTraversable(false);
        outFileButt.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            Path path = Paths.get(StringUtils.defaultIfBlank(reportFld.getText(), ""));
            if (path.toFile().exists())
                fileChooser.setInitialDirectory(Files.isDirectory(path) ? path.toFile() : path.getParent().toFile());
            File f = fileChooser.showOpenDialog(primaryStage);
            if (f != null)
                reportFld.setText(f.getAbsolutePath());
        });

        GridPane.setHalignment(reportLbl, HPos.LEFT);
        gridpane.add(reportLbl, 0, 1);

        GridPane.setHalignment(reportFld, HPos.LEFT);
        gridpane.add(reportFld, 1, 1);

        GridPane.setHalignment(outFileButt, HPos.RIGHT);
        gridpane.add(outFileButt, 2, 1);

        //****************************************************

        Label symbolNameLbl = new Label("Symbol Name *");
        final TextField symbolNameFld = new TextField();
        symbolNameFld.setText(ApplicationProperties.KEY.SymbolName.asString(""));
        symbolNameFld.setMaxWidth(100);

        GridPane.setHalignment(symbolNameLbl, HPos.LEFT);
        gridpane.add(symbolNameLbl, 0, 2);

        GridPane.setHalignment(symbolNameFld, HPos.LEFT);
        gridpane.add(symbolNameFld, 1, 2);

        //****************************************************

        Label bookDepthLbl = new Label("Book Depth (Top of Book)");
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "2",
                        "3",
                        "4",
                        "5"
                );
        final ComboBox<String> bookDepthComboBox = new ComboBox<>(options);
        bookDepthComboBox.setEditable(true);
        bookDepthComboBox.setMaxWidth(100);
        StringConverter<String> converter = new StringConverter<String>() {
            @Override
            public String toString(String object) {
                return object;
            }

            @Override
            public String fromString(String string) {
                Matcher matcher = Pattern.compile("^[ ]*([2-5])[ ]*$").matcher(string);
                String prev = bookDepthComboBox.getSelectionModel().getSelectedItem();
                return matcher.find() ? string : prev == null || prev.isEmpty() ? "2" : prev;
            }
        };
        bookDepthComboBox.setConverter(converter);
        bookDepthComboBox.getSelectionModel().select(
                ApplicationProperties.KEY.BookDepth.asInt(0));

        GridPane.setHalignment(bookDepthLbl, HPos.LEFT);
        gridpane.add(bookDepthLbl, 0, 3);

        GridPane.setHalignment(bookDepthComboBox, HPos.LEFT);
        gridpane.add(bookDepthComboBox, 1, 3);

        //****************************************************

        HBox dateTimeBox = new HBox();
        dateTimeBox.setSpacing(10);
        root.setPadding(new Insets(15, 20, 10, 10));

        Label dateLbl = new Label("Date (yyyymmdd)");
        DatePicker datePicker = new DatePicker();
        datePicker.setMaxWidth(150);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        datePicker.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                return object == null ? "" : object.format(formatter);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string, formatter);
            }
        });

        GridPane.setHalignment(dateLbl, HPos.LEFT);
        gridpane.add(dateLbl, 0, 4);

        GridPane.setHalignment(dateTimeBox, HPos.LEFT);
        gridpane.add(dateTimeBox, 1, 4);

        //****************************************************

        TextField timeFld = new TextField();
        timeFld.setMaxWidth(100);

        StringConverter<String> timeConverter = new StringConverter<String>() {
            @Override
            public String toString(String object) {
                if (object == null || object.trim().isEmpty()) {
                    timeFld.setStyle("-fx-text-fill: LightGray;");
                    return "hh:mm:ss";
                } else {
                    return object;
                }
            }

            @Override
            public String fromString(String string) {
                if (Pattern.compile("^[ ]*([0-9]{1,2}){1}(:[0-9]{1,2})*[ ]*$").matcher(string).find()) {
                    timeFld.setStyle("-fx-text-fill: black;");
                    return string;
                } else {
                    timeFld.setStyle("-fx-text-fill: red;");
                    return timeFld.getText();
                }
            }
        };
        TextFormatter<String> timeTextFormatter = new TextFormatter<>(timeConverter);
        timeFld.setTextFormatter(timeTextFormatter);
        timeFld.setOnMouseClicked(event -> timeFld.setStyle("-fx-text-fill: black;"));
        timeFld.setOnKeyReleased(event -> timeFld.setStyle("-fx-text-fill: black;"));

        dateTimeBox.getChildren().addAll(datePicker, timeFld);

        //****************************************************

        Label reportType = new Label("Report Type");
        ObservableList<String> reportTypeOptions =
                FXCollections.observableArrayList(
                        ReportBuilder.ReportFormat.CSV.name(),
                        ReportBuilder.ReportFormat.HTML.name()
                );

        final ComboBox<String> reportTypeComboBox = new ComboBox<>(reportTypeOptions);
        reportTypeComboBox.setMaxWidth(100);

        reportTypeComboBox.getSelectionModel().select(ApplicationProperties.KEY.ReportType.asInt(0));
        reportTypeComboBox.setOnAction(event -> {
            ReportBuilder.ReportFormat reportFormat = ReportBuilder.ReportFormat.valueOf(reportTypeComboBox.getSelectionModel().getSelectedItem());
            String correctOutFile = fixedOutFileExt(reportFld.getText(), reportFormat);
            reportFld.setText(correctOutFile);
        });

        GridPane.setHalignment(reportType, HPos.LEFT);
        gridpane.add(reportType, 0, 5);

        GridPane.setHalignment(reportTypeComboBox, HPos.LEFT);
        gridpane.add(reportTypeComboBox, 1, 5);

        //****************************************************

        Label algType = new Label("Algorithm");
        ObservableList<String> algTypeOptions =
                FXCollections.observableArrayList(
                        "ITR",
                        "FJP",
                        "MTH"
                );

        final ComboBox<String> algTypeComboBox = new ComboBox<>(algTypeOptions);
        algTypeComboBox.setMaxWidth(100);

        algTypeComboBox.getSelectionModel().select(0);
        algTypeComboBox.setOnAction(event -> {
            //ReportBuilder.ReportFormat reportFormat = ReportBuilder.ReportFormat.valueOf(reportTypeComboBox.getSelectionModel().getSelectedItem());
            //String correctOutFile = fixedOutFileExt(reportFld.getText(), reportFormat);
            //reportFld.setText(correctOutFile);
        });

        GridPane.setHalignment(algType, HPos.LEFT);
        gridpane.add(algType, 0, 6);

        GridPane.setHalignment(algTypeComboBox, HPos.LEFT);
        gridpane.add(algTypeComboBox, 1, 6);

        //****************************************************

        Label fltType = new Label("Filter");
        ObservableList<String> fltTypeOptions =
                FXCollections.observableArrayList(
                        "KMP",
                        "REG"
                );

        final ComboBox<String> fltTypeComboBox = new ComboBox<>(fltTypeOptions);
        fltTypeComboBox.setMaxWidth(100);

        fltTypeComboBox.getSelectionModel().select(0);
        GridPane.setHalignment(algType, HPos.LEFT);
        gridpane.add(fltType, 0, 7);

        GridPane.setHalignment(fltTypeComboBox, HPos.LEFT);
        gridpane.add(fltTypeComboBox, 1, 7);

        //****************************************************

        HBox controlBox = new HBox();
        controlBox.setPadding(new Insets(10, 10, 10, 10));
        controlBox.setSpacing(10);
        Label progressIndicatorLbl = new Label();
        progressIndicatorLbl.setVisible(false);

        ProgressIndicator progressIndicator = new ProgressIndicator(0);
        progressIndicator.setVisible(false);
        GridPane.setHalignment(progressIndicator, HPos.LEFT);
        gridpane.add(progressIndicator, 2, 5);

        Button cancelBtn = new Button("Cancel");
        Button startBtn = new Button("Start");
        startBtn.setPrefWidth(100);
        startBtn.setDefaultButton(true);
        startBtn.setDisable(isDisableStart(fixLogFileFld.getText(),
                reportFld.getText(), symbolNameFld.getText()));

        class ResetUI {
            private void resetUI() {
                startBtn.setDisable(false);
                cancelBtn.setDisable(true);
                primaryStage.getScene().setCursor(Cursor.DEFAULT);
                printThread = null;
                //progressIndicatorLbl.setVisible(false);
                progressIndicator.setVisible(false);
            }
        }

        ResetUI resetUI = new ResetUI();
        final CustomProgressIndicator customProgressIndicator = new CustomProgressIndicator(progressIndicatorLbl, progressIndicator);

        startBtn.setOnAction(event -> {
            cancelBtn.setDisable(false);
            customProgressIndicator.start();

            XMessageFilter filter;
            switch (fltTypeComboBox.getSelectionModel().getSelectedItem()) {
                case "REG":
                    filter = new my.fixj.messages_std.XMessageFilterImpl((char) 1);
                    break;
                case "KMP":
                default:
                    filter = new my.fixj.messages_kmp.XMessageFilterImpl((char) 1);
            }

            XMessageProcessor processor;
            switch (algTypeComboBox.getSelectionModel().getSelectedItem()) {
                case "MTH":
                    processor = new my.fixj.messages_std.XMessageProcessorImpl(false);
                    processor.setFilter(filter);
                    break;
                case "FJP":
                    processor = new my.fixj.messages_std.XMessageProcessorImpl(true);
                    processor.setFilter(filter);
                    break;
                case "ITR":
                default:
                    processor = new my.fixj.messages_kmp.XMessageProcessorImpl();
                    processor.setFilter(filter);
            }

            startBtn.setDisable(true);
            primaryStage.getScene().setCursor(Cursor.WAIT);

            ReportBuilder.ReportFormat reportFormat = ReportBuilder.ReportFormat.valueOf(reportTypeComboBox.getSelectionModel().getSelectedItem());
            String correctOutFile = fixedOutFileExt(reportFld.getText(), reportFormat);
            reportFld.setText(correctOutFile);

            printThread = new Thread(() -> {
                try {
                    Date sendingDate = datePicker.getValue() == null ? null : java.sql.Date.valueOf(datePicker.getValue());
                    XMessageDateTime dt = new XMessageDateTime().setDate(sendingDate)
                            .setTime(timeFld.getText());
                    processor.process(fixLogFileFld.getText(),
                            symbolNameFld.getText(),
                            dt,
                            Integer.parseInt(bookDepthComboBox.getSelectionModel().getSelectedItem()),
                            reportFld.getText(),
                            ReportBuilder.ReportFormat.valueOf(reportTypeComboBox.getSelectionModel().getSelectedItem()),
                            customProgressIndicator);
                } catch (Exception e) {
                    processor.setTerminatedProcess(true);
                    e.printStackTrace();
                } finally {
                    resetUI.resetUI();
                }
            });
            printThread.start();
        });

        fixLogFileFld.textProperty().addListener((observable, oldValue, newValue) ->
                startBtn.setDisable(isDisableStart(newValue, reportFld.getText(), symbolNameFld.getText())));

        reportFld.textProperty().addListener((observable, oldValue, newValue) ->
                startBtn.setDisable(isDisableStart(fixLogFileFld.getText(), newValue, symbolNameFld.getText())));

        symbolNameFld.textProperty().addListener((observable, oldValue, newValue) ->
                startBtn.setDisable(isDisableStart(fixLogFileFld.getText(), reportFld.getText(), newValue)));

        cancelBtn.setDisable(true);
        cancelBtn.setPrefWidth(100);
        cancelBtn.setOnAction(event -> {
            if (printThread != null) {
                printThread.interrupt();
                printThread = null;
            }
            resetUI.resetUI();
        });
        controlBox.getChildren().addAll(progressIndicatorLbl, startBtn, cancelBtn);

        root.setTop(gridpane);
        controlBox.setAlignment(Pos.BOTTOM_RIGHT);
        root.setBottom(controlBox);

        primaryStage.setScene(scene);
    }

    private boolean isDisableStart(String logFile, String reportFile, String symbolName) {
        return StringUtils.isBlank(logFile)
                || StringUtils.isBlank(reportFile) || StringUtils.isBlank(symbolName);
    }

    private String fixedOutFileExt(String outFile, ReportBuilder.ReportFormat format) {
        Path ph = Paths.get(outFile);
        Path dir = ph.getParent();
        String fn = ph.getFileName().toString();
        int pos = fn.lastIndexOf(".");
        if (pos != -1) {
            String ext = fn.substring(pos + 1);
            if (format.name().equalsIgnoreCase(ext))
                return outFile;
            String name = fn.substring(0, pos);
            return Paths.get(dir.toString(), name + "." + format.name().toLowerCase()).toString();
        } else {
            return outFile + "." + format.name().toLowerCase();
        }
    }

    public static void main(String[] args) throws org.apache.commons.cli.ParseException, IOException, ConfigurationException {
        CommandLine line = new DefaultParser().parse(new Options()
                .addOption(ApplicationProperties.OptionAdapter.Workspace.createOption())
                .addOption(ApplicationProperties.OptionAdapter.InFile.createOption())
                .addOption(ApplicationProperties.OptionAdapter.OutFile.createOption())
                .addOption(ApplicationProperties.OptionAdapter.SymbolName.createOption())
                .addOption(ApplicationProperties.OptionAdapter.DateTime.createOption()), args);

        String workspace = ApplicationProperties.OptionAdapter.Workspace.getOptionValue(line);
        String inFile = ApplicationProperties.OptionAdapter.InFile.getOptionValue(line);
        String outFile = ApplicationProperties.OptionAdapter.OutFile.getOptionValue(line);
        String symbolName = ApplicationProperties.OptionAdapter.SymbolName.getOptionValue(line);
        String dateTime = ApplicationProperties.OptionAdapter.DateTime.getOptionValue(line);

        ApplicationProperties.prepareWorkingDir(workspace, inFile, outFile, symbolName, dateTime);

        LOGGER = LoggerFactory.getLogger(MDBookReport.class);

        launch(args);
    }

    private class CustomProgressIndicator implements XMessageProcessor.ProgressIndicator, Runnable {
        private AtomicInteger countAtomic = new AtomicInteger();
        private AtomicInteger idAtomic = new AtomicInteger();
        private AtomicReference<Double> bytesAtomic = new AtomicReference<>();
        private Label label;
        private ProgressIndicator indicator;
        private long startTime;
        private DecimalFormat nf = (DecimalFormat) DecimalFormat.getInstance();

        CustomProgressIndicator(Label label, ProgressIndicator progressIndicator) {
            this.label = label;
            this.indicator = progressIndicator;
        }

        void start() {
            startTime = System.currentTimeMillis();
            nf.applyPattern("#####,000");
            countAtomic.set(0);
            idAtomic.set(0);
            bytesAtomic.set(0d);
            indicator.setProgress(0d);
            label.setText("");
            label.setVisible(true);
            indicator.setVisible(true);
        }

        @Override
        public void readMessages(int count, int id) {
            countAtomic.set(count);
            idAtomic.set(id);
            Platform.runLater(this);
        }

        @Override
        public void readBytes(double progress) {
            bytesAtomic.set(progress);
            Platform.runLater(this);
        }

        @Override
        public void run() {
            long diffTime = System.currentTimeMillis() - startTime;
            label.setText("time: " + nf.format(diffTime) + "[ms], count: " + countAtomic.get() + ", MsgSeqNum: " + idAtomic.get());
            indicator.setProgress(bytesAtomic.get());
        }
    }
}
