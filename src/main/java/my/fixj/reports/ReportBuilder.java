package my.fixj.reports;

import my.fixj.messages.XMessage;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public interface ReportBuilder {
    void begin() throws Exception;

    void add(XMessage xMessage) throws Exception;

    void commit() throws Exception;

    default void createNewFile(String outFileName) throws Exception {
        Files.deleteIfExists(Paths.get(outFileName));
        Files.createFile(Paths.get(outFileName));
    }

    default String calculateFileNameWithNewVersion(int version, String absFileName) {
        Path ph = Paths.get(absFileName);
        Path dir = ph.getParent();
        String fn = ph.getFileName().toString();
        int pos = fn.lastIndexOf(".");
        if (pos != -1) {
            String ext = fn.substring(pos + 1);
            String name = fn.substring(0, pos);
            String index = "_";
            while (Files.exists(Paths.get(dir.toString(), name + index + version), LinkOption.NOFOLLOW_LINKS))
                index += "_";
            return Paths.get(dir.toString(), name + index + version + "." + ext).toString();
        } else {
            String index = "_";
            while (Files.exists(Paths.get(dir.toString(), fn + index + version), LinkOption.NOFOLLOW_LINKS))
                index += "_";
            return Paths.get(dir.toString(), fn + index + version).toString();
        }
    }

    enum ReportFormat {
        CSV, HTML
    }
}
