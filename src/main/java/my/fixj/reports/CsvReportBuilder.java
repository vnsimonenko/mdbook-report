package my.fixj.reports;

import my.fixj.messages.XMessage;
import my.fixj.messages.XMessageCalculator;
import my.fixj.messages_kmp.XMessageCalculatorImpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://support.office.com/en-us/article/excel-specifications-and-limits-1672b34d-7043-467e-8e27-269d656771c3
 */
public class CsvReportBuilder implements ReportBuilder {
    private List<XMessage> messages;
    private String outFileName;
    private String versionOutFileName;
    private int versionOfFileName;
    private static final int NUMBER_OF_MESSAGES = 1000;
    //https://support.office.com/en-us/article/excel-specifications-and-limits-1672b34d-7043-467e-8e27-269d656771c3
    private static final int TOTAL_NUMBER_OF_ROWS = 1_048_576 - 1;
    private int allNumberOfMessages;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
    private XMessageCalculator xMessageCalculator;
    private int depth;
    private String symbolName;
    private String inputMessagesFileName;

    public CsvReportBuilder(String outFileName,
                            String inputMessagesFileName, String symbolName, int depth) {
        this.outFileName = outFileName;
        this.symbolName = symbolName;
        this.depth = depth;
        this.inputMessagesFileName = inputMessagesFileName;
    }

    public void begin() throws Exception {
        allNumberOfMessages = 4;
        versionOfFileName = 2;
        versionOutFileName = outFileName;
        createNewFile(outFileName);
        try {
            StringBuilder head = new StringBuilder();
            head.append(",,,,,,,,,,,,,,,,Market Data Incremental Refresh\n");
            head.append(",,,,,,,,,,,,,,,Log file, ");
            head.append(inputMessagesFileName);
            head.append("\n");
            head.append(",,,,,,,,,,,,,,,Symbol Name, ");
            head.append(symbolName);
            head.append("\n");
            head.append(",,,,,,,,,,,,,,,Book depth, ");
            head.append(depth);
            head.append("\n");

            Files.write(Paths.get(outFileName), head.toString().getBytes(), StandardOpenOption.APPEND);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        messages = new ArrayList<>(NUMBER_OF_MESSAGES);
        xMessageCalculator = new XMessageCalculatorImpl();
    }

    public void add(XMessage xMessage) throws Exception {
        if (messages.size() > NUMBER_OF_MESSAGES) {
            flush();
        }
        if (xMessage != null) {
            messages.add(xMessage);
            //allNumberOfMessages += 3 + xMessage.getXEntries().size();
        }
    }

    public void commit() throws Exception {
        if (messages.size() > 0) {
            flush();
        }
    }

    private class WordPosition {
        Map<Integer, Map<Integer, String>> datas = new HashMap<>();

        void write(int row, int col, String word) {
            Map<Integer, String> data = datas.computeIfAbsent(row, key -> new HashMap<>());
            data.put(col, word);
        }
    }

    private void flush() throws Exception {
        WordPosition wp = new WordPosition();
        int max_rows = 1;
        int row = 1;
        for (XMessage msg : messages) {
            wp.write(row, 0, Integer.toString(msg.getMsgSeqNum()));
            wp.write(row, 13, "Sending Time");
            wp.write(row, 14, dateFormat.format(msg.getSendingTime()));
            row++;
            wp.write(row, 13, "Receiving Time");
            wp.write(row, 14, dateFormat.format(msg.getReceiveDateTime()));
            row++;
            wp.write(row, 13, "Difference(ms)");
            wp.write(row, 14, "" + (msg.getReceiveDateTime().getTime() - msg.getSendingTime().getTime()));
            row++;
            wp.write(row, 0, "");
            row++;
            wp.write(row, 0, "ID");
            wp.write(row, 1, "Action");
            wp.write(row, 2, "Side");
            wp.write(row, 3, "Price");
            wp.write(row, 4, "Size");
            wp.write(row, 5, "");
            wp.write(row, 6, "BID-Price");
            wp.write(row, 7, "BID-Size");
            wp.write(row, 8, "ASK-Price");
            wp.write(row, 9, "ASK-Size");
            wp.write(row, 10, "");
            wp.write(row, 11, "Action");
            wp.write(row, 12, "Side");
            wp.write(row, 13, "Price");
            wp.write(row, 14, "Size");
            int startRow = ++row;
            for (XMessage.XEntry ent : msg.getXEntries()) {
                wp.write(row, 0, ent.getMDEntryID());
                wp.write(row, 1, ent.isDeletedByMDUpdateAction() ? "Delete" : "New");
                wp.write(row, 2, ent.isDeletedByMDUpdateAction() ? "" : ent.getMDEntryType().name());
                wp.write(row, 3, ent.isDeletedByMDUpdateAction() ? "" : ent.getMDEntryPx().toString());
                wp.write(row, 4, ent.isDeletedByMDUpdateAction() ? "" : ent.getMDEntrySize().toString());
                wp.write(row, 5, "");
                row++;
            }
            max_rows = Math.max(row, max_rows);

            XMessageCalculatorImpl.OrderBookSnapshot result = xMessageCalculator.calculate(msg, depth);

            row = startRow;
            for (XMessageCalculatorImpl.PrintXEntry ent : result.getBids()) {
                wp.write(row, 6, ent.getPrice().toString());
                wp.write(row++, 7, ent.getSize().toString());
            }
            row = startRow + result.getBids().size();
            for (XMessageCalculatorImpl.PrintXEntry ent : result.getAsks()) {
                wp.write(row, 8, ent.getPrice().toString());
                wp.write(row++, 9, ent.getSize().toString());
            }
            max_rows = Math.max(row, max_rows);

            row = startRow;
            for (XMessageCalculatorImpl.PrintXEntry ent : result.getActions()) {
                wp.write(row, 11, ent.getState().name());
                wp.write(row, 12, ent.getType().toString());
                wp.write(row, 13, ent.getPrice().toString());
                wp.write(row++, 14, ent.getSize().toString());
            }

            max_rows = Math.max(row, max_rows);
            wp.write(++max_rows, 0, "");
            row = ++max_rows;
        }

        StringBuilder sb = new StringBuilder(100);
        for (row = 0; row < max_rows; row++) {
            for (int col = 0; col < 15; col++) {
                if (wp.datas.containsKey(row)) {
                    Map<Integer, String> data = wp.datas.get(row);
                    if (data.containsKey(col)) {
                        sb.append(data.get(col));
                        sb.append(", ");
                    } else {
                        sb.append(", ");
                    }
                } else {
                    for (int col1 = 0; col1 < 15; col1++) {
                        sb.append(",");
                    }
                }
            }
            sb.append("\n");
            allNumberOfMessages++;

            if (allNumberOfMessages == TOTAL_NUMBER_OF_ROWS) {
                String report = sb.toString();
                saveToFile(report);
                sb.delete(0, report.length());

                versionOutFileName = calculateFileNameWithNewVersion(versionOfFileName, outFileName);
                createNewFile(versionOutFileName);
                versionOfFileName++;
                allNumberOfMessages = 0;
            }
        }

        if (sb.length() > 0) {
            saveToFile(sb.toString());
        }

        messages.clear();
    }

    private void saveToFile(String report) {
        try {
            Files.write(Paths.get(versionOutFileName), report.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
