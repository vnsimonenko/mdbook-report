package my.fixj.reports;

import my.fixj.messages.XMessage;
import my.fixj.messages.XMessageCalculator;
import my.fixj.messages_kmp.XMessageCalculatorImpl;
import net.sf.saxon.s9api.SaxonApiException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class HtmlReportBuilder implements ReportBuilder {
    private List<XMessage> xMessages;
    private String outFileName;
    private int versionOfFileName;
    private static final int NUMBER_OF_MESSAGES = 10000;
    private SaxonTransformer saxonTransformer;
    private JAXBContext jaxbContext;
    private XMessageCalculator xMessageCalculator;
    private int depth;
    private String symbolName;
    private String inputMessagesFileName;

    public HtmlReportBuilder(String outFileName,
                             String inputMessagesFileName, String symbolName, int depth) throws JAXBException, SaxonApiException {
        this.outFileName = outFileName;
        xMessages = new ArrayList<>();
        saxonTransformer = new SaxonTransformer("/xslt/report.xsl");
        jaxbContext = JAXBContext.newInstance(Messages.class);
        xMessageCalculator = new XMessageCalculatorImpl();
        this.symbolName = symbolName;
        this.depth = depth;
        this.inputMessagesFileName = inputMessagesFileName;
    }

    private void flush() throws Exception {
        String versionOutFileName = (versionOfFileName > 1)
                ? calculateFileNameWithNewVersion(versionOfFileName, outFileName)
                : outFileName;
        versionOfFileName++;

        Messages messages = new Messages();
        messages.setLogFile(inputMessagesFileName);
        messages.setBookDepth(depth);
        messages.setSymbolName(symbolName);
        for (XMessage xMessage : xMessages) {
            XMessageCalculatorImpl.OrderBookSnapshot result = xMessageCalculator.calculate(xMessage, depth);
            messages.getMessages().add(toXml(xMessage, result));
        }
        xMessages.clear();

        JAXBSource source = new JAXBSource(jaxbContext, messages);
        saxonTransformer.transform(source, versionOutFileName);
    }

    private Message toXml(XMessage xMessage, XMessageCalculatorImpl.OrderBookSnapshot orderBookSnapshot) {
        Message message = new Message();
        message.setSendingDate(xMessage.getSendingTime());
        message.setReceiveDate(xMessage.getReceiveDateTime());
        message.setDifference(xMessage.getReceiveDateTime().getTime() - xMessage.getSendingTime().getTime());
        message.setMsgSeqNum(xMessage.getMsgSeqNum());
        for (XMessageCalculatorImpl.PrintXEntry ent : orderBookSnapshot.getBids()) {
            message.getBidBooks().add(new OrderBook(ent.getPrice(), ent.getSize()));
        }
        for (XMessageCalculatorImpl.PrintXEntry ent : orderBookSnapshot.getAsks()) {
            message.getAskBooks().add(new OrderBook(ent.getPrice(), ent.getSize()));
        }
        for (XMessageCalculatorImpl.PrintXEntry ent : orderBookSnapshot.getActions()) {
            message.getActOrderBooks().add(
                    new ActOrderBook(ent.getPrice(), ent.getSize(), ent.getType().name(), ent.getState().name()));
        }
        for (XMessage.XEntry xEnt : xMessage.getXEntries()) {
            Entry ent = new Entry();
            ent.setId(xEnt.getMDEntryID());
            if (!xEnt.isDeletedByMDUpdateAction()) {
                ent.setPrice(xEnt.getMDEntryPx());
                ent.setSize(xEnt.getMDEntrySize());
                ent.setSide(xEnt.getMDEntryType().name());
                ent.setAction("New");
            } else {
                ent.setAction("Delete");
            }
            message.getEntries().add(ent);
        }
        return message;
    }

    @Override
    public void begin() throws Exception {
        versionOfFileName = 1;
        createNewFile(outFileName);
        try {
            Files.write(Paths.get(outFileName), "<messages_kmp>\n".getBytes(), StandardOpenOption.APPEND);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void add(XMessage xMessage) throws Exception {
        if (xMessages.size() > NUMBER_OF_MESSAGES) {
            flush();
        }
        if (xMessage != null) {
            xMessages.add(xMessage);
        }
    }

    @Override
    public void commit() throws Exception {
        if (xMessages.size() > 0) {
            flush();
        }
    }
}
