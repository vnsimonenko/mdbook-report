package my.fixj.reports;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.InputStream;

/**
 * @see "http://www.saxonica.com/documentation/index.html"
 * @see "https://www.w3.org/TR/xslt20/#function-format-dateTime"
 */
class SaxonTransformer {

    private final Processor processor;
    private final XsltExecutable xsltExec;

    SaxonTransformer(final String xslFileName) throws SaxonApiException {
        InputStream resourceAsStream = SaxonTransformer.class.getResourceAsStream(xslFileName);
        StreamSource xslt = new StreamSource(resourceAsStream);
        processor = new Processor(false);
        xsltExec = processor.newXsltCompiler().compile(xslt);
    }

    /**
     * @see "http://www.saxonica.com/documentation/index.html#!javadoc/net.sf.saxon.s9api/Serializer"
     */
    void transform(final Source source, final String outFileName) throws Exception {
        final XsltTransformer transformer = xsltExec.load();
        transformer.setSource(source);
        processor.newSerializer(new File(outFileName));
        transformer.setDestination(processor.newSerializer(new File(outFileName)));
        transformer.transform();
    }
}