package my.fixj.reports;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement
public class Messages {
    @XmlElement(name = "start")
    private Date start;
    @XmlElement(name = "finish")
    private Date finish;
    @XmlElement(name = "log_name")
    private String logFile;
    @XmlElement(name = "symbol_name")
    private String symbolName;
    @XmlElement(name = "book_depth")
    private Integer bookDepth;

    @XmlElementWrapper(name = "messages_kmp")
    @XmlElement(name = "message")
    private List<Message> messages;

    public Messages() {
        this.messages = new ArrayList<>();
    }

    public void setFinish(Date finish) {
        this.finish = finish;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public void setLogFile(String logFile) {
        this.logFile = logFile;
    }

    public void setBookDepth(Integer bookDepth) {
        this.bookDepth = bookDepth;
    }

    public void setSymbolName(String symbolName) {
        this.symbolName = symbolName;
    }

    public List<Message> getMessages() {
        return messages;
    }
}

@XmlAccessorType(XmlAccessType.FIELD)
class Message {
    @XmlElement(name = "sendingDate")
    private Date sendingDate;
    @XmlElement(name = "receiveDate")
    private Date receiveDate;
    @XmlElement(name = "difference")
    private long difference;
    @XmlElement(name = "msgSeqNum")
    private int msgSeqNum;

    @XmlElementWrapper(name = "bidBooks")
    @XmlElement(name = "bidBook")
    private List<OrderBook> bidBooks;
    @XmlElementWrapper(name = "askBooks")
    @XmlElement(name = "askBook")
    private List<OrderBook> askBooks;
    @XmlElementWrapper(name = "actOrderBooks")
    @XmlElement(name = "actOrderBook")
    private List<ActOrderBook> actOrderBooks;

    @XmlElementWrapper(name = "entries")
    @XmlElement(name = "entry")
    private List<Entry> entries;

    public Message() {
        this.entries = new ArrayList<>();
        this.bidBooks = new ArrayList<>();
        this.askBooks = new ArrayList<>();
        this.actOrderBooks = new ArrayList<>();
    }

    public void setMsgSeqNum(int msgSeqNum) {
        this.msgSeqNum = msgSeqNum;
    }

    public Date getSendingDate() {
        return sendingDate;
    }

    public void setSendingDate(Date sendingDate) {
        this.sendingDate = sendingDate;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public void setDifference(long difference) {
        this.difference = difference;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public List<OrderBook> getBidBooks() {
        return bidBooks;
    }

    public List<OrderBook> getAskBooks() {
        return askBooks;
    }

    public List<ActOrderBook> getActOrderBooks() {
        return actOrderBooks;
    }
}

@XmlAccessorType(XmlAccessType.FIELD)
class Entry {
    @XmlElement(name = "id")
    private String id;
    @XmlElement(name = "action")
    private String action;
    @XmlElement(name = "side")
    private String side;
    @XmlElement(name = "price")
    private BigDecimal price;
    @XmlElement(name = "size")
    private BigDecimal size;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }
}

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class OrderBook {
    @XmlElement(name = "price")
    private BigDecimal price;
    @XmlElement(name = "size")
    private BigDecimal size;

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getSize() {
        return size;
    }

    public OrderBook(BigDecimal price, BigDecimal size) {
        this.price = price;
        this.size = size;
    }

    public OrderBook() {
    }
}

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class ActOrderBook {
    @XmlElement(name = "price")
    private BigDecimal price;
    @XmlElement(name = "size")
    private BigDecimal size;
    @XmlElement(name = "side")
    private String side;
    @XmlElement(name = "action")
    private String action;

    public ActOrderBook() {
    }

    public ActOrderBook(BigDecimal price, BigDecimal size, String side, String action) {
        this.price = price;
        this.size = size;
        this.side = side;
        this.action = action;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getSize() {
        return size;
    }

    public String getSide() {
        return side;
    }

    public String getAction() {
        return action;
    }
}