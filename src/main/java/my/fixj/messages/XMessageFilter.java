package my.fixj.messages;

import my.fixj.messages_kmp.XMessageDateTime;

import java.io.IOException;

public interface XMessageFilter {
    boolean filter(String fileName, String symbolName, XMessageDateTime sendingDate, MessageTextHandler handler) throws IOException;
    XMessageParser getParser();

    interface MessageTextHandler {
        void handle(String message);
        void readBytes(double progress);
    }
}


