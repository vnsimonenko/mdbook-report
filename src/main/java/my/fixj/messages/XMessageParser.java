package my.fixj.messages;

import java.text.ParseException;

public interface XMessageParser {
    XMessage parse(String message) throws ParseException;
}
