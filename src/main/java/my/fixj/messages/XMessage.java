package my.fixj.messages;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;

public class XMessage {
    private int msgSeqNum;
    private Date sendingTime;
    private Date receiveDateTime;
    //private int NoMDEntries; //268
    private LinkedList<XEntry> xEntries;

    public XMessage() {
    }

    public int getMsgSeqNum() {
        return msgSeqNum;
    }

    public void setMsgSeqNum(int msgSeqNum) {
        this.msgSeqNum = msgSeqNum;
    }

    public Date getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(Date sendingTime) {
        this.sendingTime = sendingTime;
    }

    public Date getReceiveDateTime() {
        return receiveDateTime;
    }

    public void setReceiveDateTime(Date receiveDateTime) {
        this.receiveDateTime = receiveDateTime;
    }

    public LinkedList<XEntry> getXEntries() {
        if (xEntries == null)
            xEntries = new LinkedList<>();
        return xEntries;
    }

    public XEntry getLastXEntry() {
        return getXEntries().getLast();
    }

    public XEntry addXEntry() {
        XEntry xEntry = new XEntry();
        this.getXEntries().add(xEntry);
        return xEntry;
    }

    public enum MDEntryType {
        BID, ASK
    }

    public static class XEntry {
        private int MDUpdateAction; //279
        private MDEntryType MDEntryType; //269
        private String MDEntryID; //278
        private String Symbol; //55
        private BigDecimal MDEntryPx = BigDecimal.ZERO; //270
        private BigDecimal MDEntrySize = BigDecimal.ZERO; //271

        private XEntry() {
        }

        public void setMDUpdateAction(int MDUpdateAction) {
            this.MDUpdateAction = MDUpdateAction;
        }

        public MDEntryType getMDEntryType() {
            return MDEntryType;
        }

        public void setMDEntryType(MDEntryType MDEntryType) {
            this.MDEntryType = MDEntryType;
        }

        public String getMDEntryID() {
            return MDEntryID;
        }

        public void setMDEntryID(String MDEntryID) {
            this.MDEntryID = MDEntryID;
        }

        public String getSymbol() {
            return Symbol;
        }

        public void setSymbol(String symbol) {
            Symbol = symbol;
        }

        public BigDecimal getMDEntryPx() {
            return MDEntryPx;
        }

        public void setMDEntryPx(BigDecimal MDEntryPx) {
            this.MDEntryPx = MDEntryPx;
        }

        public BigDecimal getMDEntrySize() {
            return MDEntrySize;
        }

        public void setMDEntrySize(BigDecimal MDEntrySize) {
            this.MDEntrySize = MDEntrySize;
        }

        public boolean isDeletedByMDUpdateAction() {
            return MDUpdateAction == 2;
        }
    }
}
