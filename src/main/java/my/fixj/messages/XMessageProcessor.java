package my.fixj.messages;

import my.fixj.messages_kmp.XMessageDateTime;
import my.fixj.reports.ReportBuilder;

public interface XMessageProcessor {
    int process(String inputMessagesFileName,
                 String symbolName,
                 XMessageDateTime sending,
                 int depth,
                 String reportFileName,
                 ReportBuilder.ReportFormat reportFormat,
                 ProgressIndicator indicator) throws Exception;

    void setFilter(XMessageFilter filter);

    void setTerminatedProcess(boolean isTerminatedProcess);

    interface ProgressIndicator {
        void readMessages(int count, int id);

        void readBytes(double progress);
    }
}
