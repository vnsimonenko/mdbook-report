package my.fixj.messages;

import my.fixj.messages_kmp.XMessageCalculatorImpl;

import java.math.BigDecimal;
import java.util.*;

public interface XMessageCalculator {
    XMessageCalculatorImpl.OrderBookSnapshot calculate(XMessage xMessage, int depth);

    interface OrderBookSnapshot {

        Collection<XMessageCalculatorImpl.PrintXEntry> getBids();

        Collection<XMessageCalculatorImpl.PrintXEntry> getAsks();

        List<XMessageCalculatorImpl.PrintXEntry> getActions();
    }

    enum State {
        None, New, Update, Delete
    }

    class PrintXEntry {
        private final String id;
        private final BigDecimal price;
        private final BigDecimal size;
        private final XMessage.MDEntryType type;
        private State state = State.New;

        public PrintXEntry(XMessage.XEntry xent) {
            this.id = xent.getMDEntryID();
            this.price = xent.getMDEntryPx();
            this.size = xent.getMDEntrySize();
            this.type = xent.getMDEntryType();
        }

        public String getId() {
            return id;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public BigDecimal getSize() {
            return size;
        }

        public XMessageCalculatorImpl.State getState() {
            return state;
        }

        public XMessage.MDEntryType getType() {
            return type;
        }

        public void setState(State state) {
            this.state = state;
        }
    }
}
