package my.fixj;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface ApplicationProperties {
    Map<KEY, Object> REGISTRATOR = new HashMap<>();

    static void prepareWorkingDir(String workspace, String inFile, String outFile, String symbolName, String dateTime) throws IOException, ConfigurationException {
        Path workingDirPath;
        if (StringUtils.isBlank(workspace)) {
            String userHomeDir = System.getProperty("user.home");
            boolean isWin = System.getProperty("os.name").startsWith("win");
            String dir = "market-data-book";
            workingDirPath = isWin ? Paths.get(userHomeDir, dir) : Paths.get(userHomeDir, "." + dir);
        } else {
            workingDirPath = Paths.get(workspace);
        }
        if (!Files.exists(workingDirPath, LinkOption.NOFOLLOW_LINKS)) {
            Files.createDirectories(workingDirPath);
        }
        System.setProperty("workspace", workingDirPath.toAbsolutePath().toString());
        Path applicationPropertiesPath = Paths.get(workingDirPath.toAbsolutePath().toString(), "application.properties");
        if (!Files.exists(applicationPropertiesPath, LinkOption.NOFOLLOW_LINKS)) {
            InputStream in = ApplicationProperties.class.getClassLoader().getResourceAsStream("application.properties");
            Files.copy(in, applicationPropertiesPath);
        }
        PropertiesConfiguration.setDefaultListDelimiter(';');
        PropertiesConfiguration configuration = new PropertiesConfiguration(applicationPropertiesPath.toFile());
        configuration.setProperty("workspace", workingDirPath.toAbsolutePath().toString());
        if (!StringUtils.isBlank(inFile))
            configuration.setProperty("in_file", inFile);
        if (!StringUtils.isBlank(outFile))
            configuration.setProperty("out_file", outFile);
        if (!StringUtils.isBlank(symbolName))
            configuration.setProperty("symbol_name", symbolName);
        if (!StringUtils.isBlank(dateTime))
            configuration.setProperty("date_time", dateTime);

        REGISTRATOR.put(KEY.Configuration, configuration);
    }

    default String name() {
        return null;
    }

    default Configuration getConfiguration() {
        return (Configuration) REGISTRATOR.get(KEY.Configuration);
    }

    default String asString(String... def) {
        return getConfiguration().getString(name(), def.length == 0 ? "" : def[0]);
    }

    default Integer asInt(Integer... def) {
        return getConfiguration().getInt(name(), def.length == 0 ? 0 : def[0]);
    }

    default List<Object> asList(Object... def) {
        return getConfiguration().getList(name(), Arrays.asList(def));
    }

    default List<Integer> asIntegerList(Object... def) {
        return getConfiguration().getList(name(), Arrays.asList(def)).stream().map(obj -> obj instanceof Integer ? (Integer) obj : Integer.valueOf(obj.toString())).collect(Collectors.toList());
    }

    enum KEY implements ApplicationProperties {
        Configuration,
        Workspace,
        LogFile,
        InFile,
        OutFile,
        ReportType,
        SymbolName,
        BookDepth,
        date_time
    }

    enum OptionAdapter {
        Workspace("workspace"),
        InFile("in_file"),
        OutFile("out_file"),
        SymbolName("symbol_name"),
        DateTime("date_time");

        private String name;

        OptionAdapter(String name) {
            this.name = name;
        }

        public String getOptionValue(CommandLine commandLine) {
            return commandLine.getOptionValue(name, "");
        }

        @SuppressWarnings("static-access")
        public Option createOption() {
            return OptionBuilder.withLongOpt(name)
                    .hasArg()
                    .withValueSeparator('=')
                    .create();
        }
    }
}
