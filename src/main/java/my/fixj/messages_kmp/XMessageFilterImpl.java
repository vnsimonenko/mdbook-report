package my.fixj.messages_kmp;

import my.fixj.KMP;
import my.fixj.messages.XMessageFilter;
import my.fixj.messages.XMessageParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class XMessageFilterImpl implements XMessageFilter {
    private XMessageParserImpl parser;

    public XMessageFilterImpl(char splitter) {
        //splitter is 1
        //without setup
    }

    @Override
    public boolean filter(String fileName, String symbolName, XMessageDateTime sendingDate, XMessageFilter.MessageTextHandler handler) throws IOException {
        Map<String, Object> atts = Files.readAttributes(Paths.get(fileName), "size");
        long allBytes = (Long) atts.get("size");
        //Closeable
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName), 65536) {
            double readBytes;
            int period = 0;

            @Override
            public int read() throws IOException {
                if (period > 10000) {
                    handler.readBytes(readBytes / allBytes);
                    period = 0;
                }
                readBytes++;
                period++;
                return super.read();
            }
        }) {
            char[] pattern0 = {'\n'};
            char[] pattern1 = {':', 'I', ':', 'N', ' '};
            char[] pattern2 = {' ', '8', '=', 'F', 'I', 'X', '.', '4', '.', '4', 1};
            char[] pattern3 = {1, '3', '5', '='};
            char[] pattern4 = {1};
            char[] pattern5 = {1, '5', '2', '='};
            char[] pattern6 = {'.'};
            char[] pattern7 = {1, '5', '5', '='};
            char[] pattern8 = {1};
            char[] pattern9 = {1, '1', '0', '='};
            char[] pattern10 = {1};
            KMP kmp0 = new KMP(pattern0, 256);
            KMP kmp1 = new KMP(pattern1, 256);
            KMP kmp2 = new KMP(pattern2, 256);
            KMP kmp3 = new KMP(pattern3, 256);
            KMP kmp4 = new KMP(pattern4, 256);
            KMP kmp5 = new KMP(pattern5, 256);
            KMP kmp6 = new KMP(pattern6, 256);
            KMP kmp7 = new KMP(pattern7, 256);
            KMP kmp8 = new KMP(pattern8, 256);
            KMP kmp9 = new KMP(pattern9, 256);
            KMP kmp10 = new KMP(pattern10, 256);
            String expectedSendingDate = sendingDate.toString();
            StringBuilder sb = new StringBuilder(1024);
            final int currencyTextLength = ("=" + symbolName.trim()).length();
            boolean b = false;
            for (; ; ) {
                if (kmp0.searchAndFill(reader, null) == -1)
                    break;
                if (kmp1.searchAndFill(reader, null) == -1)
                    break;
                if (kmp2.searchAndFill(reader, sb) == -1)
                    break;
                if (kmp3.searchAndFill(reader, sb) != -1 && kmp4.searchAndFill(reader, sb) != -1) {
                    if ("=X".equals(sb.substring(sb.length() - 3, sb.length() - 1))) {
                        if (expectedSendingDate == null || kmp5.searchAndFill(reader, sb) != -1 && kmp6.searchAndFill(reader, sb) != -1
                                && (sb.substring(sb.length() - 18, sb.length() - 1)).startsWith(expectedSendingDate)) {
                            if (kmp7.searchAndFill(reader, sb) != -1 && kmp8.searchAndFill(reader, sb) != -1
                                    && symbolName.equals(sb.substring(sb.length() - currencyTextLength, sb.length() - 1))) {
                                if (kmp9.searchAndFill(reader, sb, 1) != -1 && kmp10.searchAndFill(reader, sb) != -1) {
                                    handler.handle(sb.toString());
                                    b = true;
                                }
                            }
                        }
                    }
                }
                sb.setLength(0);
            }
            handler.handle(null);
            return b;
        }
    }

    @Override
    public XMessageParser getParser() {
        if (parser == null)
            parser = new XMessageParserImpl();
        return parser;
    }
}
