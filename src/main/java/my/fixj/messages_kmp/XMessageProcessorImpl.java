package my.fixj.messages_kmp;

import my.fixj.messages.XMessageFilter;
import my.fixj.messages.XMessageParser;
import my.fixj.messages.XMessageProcessor;
import my.fixj.reports.CsvReportBuilder;
import my.fixj.reports.HtmlReportBuilder;
import my.fixj.reports.ReportBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static my.fixj.messages_kmp.XMessageContainer.LinkedItem;

public class XMessageProcessorImpl implements XMessageProcessor {
    private XMessageContainer xMessageContainer;
    private ReportBuilder reportBuilder;
    private ProgressIndicator progressIndicator;
    private volatile boolean isTerminatedProcess;
    private final static Logger LOGGER = LoggerFactory.getLogger(XMessageProcessorImpl.class);
    private XMessageFilter xMessageFilter;
    private XMessageParser parser;

    public int process(String inputMessagesFileName,
                       String symbolName,
                       XMessageDateTime sending,
                       int depth,
                       String reportFileName,
                       ReportBuilder.ReportFormat reportFormat,
                       XMessageProcessor.ProgressIndicator indicator) throws Exception {

        isTerminatedProcess = false;
        progressIndicator = indicator;
        xMessageContainer = new XMessageContainer();
        if (xMessageFilter == null) {
            xMessageFilter = new XMessageFilterImpl((char) 1);
        }

        parser = xMessageFilter.getParser();

        switch (reportFormat) {
            case HTML:
                reportBuilder = new HtmlReportBuilder(reportFileName,
                        inputMessagesFileName, symbolName, depth);
                break;
            case CSV:
            default:
                reportBuilder = new CsvReportBuilder(reportFileName,
                        inputMessagesFileName, symbolName, depth);
        }

        int nThread = Runtime.getRuntime().availableProcessors();
        ExecutorService service = Executors.newFixedThreadPool(nThread);
        List<Future> tasks = new ArrayList<>();
        Iterator<XMessageContainer.LinkedItem> it = xMessageContainer.iterator();
        while (nThread-- > 0) {
            tasks.add(service.submit(new MessageParserRunnable(it)));
        }
        ExecutorService reportService = Executors.newSingleThreadExecutor();
        it = xMessageContainer.iterator(Objects::nonNull);
        Future<Integer> reportTask = reportService.submit(new MessageReportRunnable(it));
        if (!xMessageFilter.filter(inputMessagesFileName, symbolName, sending, new MessageTextHandler())) {
            printInfo(xMessageContainer.size(), 0,
                    inputMessagesFileName, symbolName, sending, depth, reportFileName, reportFormat);
            return 0;
        }
        for (Future task : tasks) task.get();

        service.shutdown();
        reportService.shutdown();

        int numberOfReportMessages = reportTask.get();

        printInfo(xMessageContainer.size(), reportTask.get(),
                inputMessagesFileName, symbolName, sending, depth, reportFileName, reportFormat);

        return numberOfReportMessages;
    }

    @Override
    public void setFilter(XMessageFilter filter) {
        xMessageFilter = filter;
    }

    private boolean isTerminatedProcess() {
        return isTerminatedProcess || Thread.currentThread().isInterrupted();
    }

    class MessageTextHandler implements XMessageFilter.MessageTextHandler {

        @Override
        public void handle(String message) {
            if (message == null)
                xMessageContainer.complete();
            else
                xMessageContainer.add(message);
        }

        @Override
        public void readBytes(double progress) {
            progressIndicator.readBytes(progress);
        }
    }

    class MessageParserRunnable implements Runnable {
        private Iterator<LinkedItem> it;

        MessageParserRunnable(Iterator<LinkedItem> it) {
            this.it = it;
        }

        public void run() {
            try {
                LinkedItem li;
                while (!isTerminatedProcess() && (li = it.next()) != null) {
                    li.setXMessage(parser.parse(li.getMessage()));
                }
            } catch (ParseException ex) {
                isTerminatedProcess = true;
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }

    class MessageReportRunnable implements Callable<Integer> {
        int nXMessage = 0;
        private Iterator<LinkedItem> it;

        MessageReportRunnable(Iterator<LinkedItem> it) {
            this.it = it;
        }

        public Integer call() throws Exception {
            try {
                LinkedItem li = it.next();
                reportBuilder.begin();
                reportBuilder.add(li.getXMessage());
                nXMessage++;
                if (progressIndicator != null)
                    progressIndicator.readMessages(nXMessage + 1, li.getXMessage().getMsgSeqNum());
                while (!isTerminatedProcess() && (li = it.next()) != null) {
                    reportBuilder.add(li.getXMessage());
                    if (nXMessage == 1)
                        xMessageContainer.clearHead(); //gc clear linked list of xmessages
                    nXMessage++;
                    if (progressIndicator != null)
                        progressIndicator.readMessages(nXMessage, li.getXMessage().getMsgSeqNum());
                }
                reportBuilder.commit();
            } catch (InterruptedException ex) {
                isTerminatedProcess = true;
                LOGGER.error(ex.getMessage(), ex);
            }
            return nXMessage;
        }
    }

    public void setTerminatedProcess(boolean isTerminatedProcess) {
        this.isTerminatedProcess = isTerminatedProcess;
    }

    private void printInfo(int numberOfLoadMessagesProcessed,
                           int numberOfReportMessagesProcessed,
                           String inputMessagesFileName,
                           String symbolName,
                           XMessageDateTime sending,
                           int depth,
                           String reportFileName,
                           ReportBuilder.ReportFormat reportFormat) {
        String info = "\n********** INFO **********"
                + "\ninput file name: " + inputMessagesFileName
                + "\nreport file name: " + reportFileName
                + "\nsymbol name: " + symbolName
                + "\nsending: " + sending
                + "\ndepth: " + depth
                + "\nreport format: " + reportFormat
                + "\nnumberOfParsedMessagesProcessed: " + numberOfLoadMessagesProcessed
                + "\nnumberOfReportMessagesProcessed: " + numberOfReportMessagesProcessed
                + "\nstatus: " + (numberOfLoadMessagesProcessed == numberOfReportMessagesProcessed ? "success" : "fail")
                + "\n**********";
        LOGGER.info(info);
    }
}
