package my.fixj.messages_kmp;

import my.fixj.messages.XMessage;
import my.fixj.messages.XMessageCalculator;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class XMessageCalculatorImpl implements XMessageCalculator {
    private OrderBookSnapshotImpl orderBookSnapshot = new OrderBookSnapshotImpl();

    public OrderBookSnapshot calculate(XMessage xMessage, int depth) {
        orderBookSnapshot.actions.clear();
        Map<String, PrintXEntry> prevBigEntries = orderBookSnapshot.bids.values().stream()
                .collect(Collectors.toMap(PrintXEntry::getId, v -> v));
        Map<String, PrintXEntry> prevAskEntries = orderBookSnapshot.asks.values().stream()
                .collect(Collectors.toMap(PrintXEntry::getId, v -> v));

        for (XMessage.XEntry xent : xMessage.getXEntries()) {
            if (xent.isDeletedByMDUpdateAction()) {
                PrintXEntry entry = prevBigEntries.get(xent.getMDEntryID());
                if (entry != null) {
                    entry.setState(State.Delete);
                    orderBookSnapshot.actions.add(entry);
                    orderBookSnapshot.bids.remove(entry.getPrice());
                    prevBigEntries.remove(entry.getId());
                }
                entry = prevAskEntries.get(xent.getMDEntryID());
                if (entry != null) {
                    entry.setState(State.Delete);
                    orderBookSnapshot.actions.add(entry);
                    orderBookSnapshot.asks.remove(entry.getPrice());
                    prevAskEntries.remove(entry.getId());
                }
                if (orderBookSnapshot.bids.size() == 0 && orderBookSnapshot.asks.size() == 0)
                    break;
            }
        }

        int bidDepth = 0;
        int askDepth = 0;
        for (XMessage.XEntry xent : xMessage.getXEntries()) {
            if (xent.getMDEntryType() == XMessage.MDEntryType.BID) {
                if (bidDepth == depth)
                    continue;
                bidDepth++;
                PrintXEntry printXEntry = new PrintXEntry(xent);
                orderBookSnapshot.bids.put(printXEntry.getPrice(), printXEntry);
            } else if (xent.getMDEntryType() == XMessage.MDEntryType.ASK) {
                if (askDepth == depth)
                    continue;
                askDepth++;
                PrintXEntry printXEntry = new PrintXEntry(xent);
                orderBookSnapshot.asks.put(printXEntry.getPrice(), printXEntry);
            }
        }

        while (orderBookSnapshot.bids.size() > depth) {
            BigDecimal price = orderBookSnapshot.bids.firstKey();
            orderBookSnapshot.bids.remove(price);
        }

        while (orderBookSnapshot.asks.size() > depth) {
            BigDecimal price = orderBookSnapshot.asks.lastKey();
            orderBookSnapshot.asks.remove(price);
        }

        //Изменилась запись или нет можно было бы определить сравнивая ключ (k) и значение (v)
        //в TreeMap<PrintXEntry, PrintXEntry>,
        //например:
        //TreeMap<PrintXEntry, PrintXEntry> map = new ...;
        //ent1 = new ent(k1,v1);
        //ent2 = new ent(k1,v2);
        //map.put(ent1, ent1);
        //map.put(ent2, ent2);
        //map.size == 1 =>
        //map.firstkey == ent1
        //map.get(map.firstkey) == ent2
        for (PrintXEntry entry : prevBigEntries.values()) {
            PrintXEntry dup = orderBookSnapshot.bids.get(entry.getPrice());
            if (dup != null)
                dup.setState(!dup.getId().equalsIgnoreCase(entry.getId()) ? State.Update : State.None);
        }
        for (PrintXEntry entry : prevAskEntries.values()) {
            PrintXEntry dup = orderBookSnapshot.asks.get(entry.getPrice());
            if (dup != null)
                dup.setState(!dup.getId().equalsIgnoreCase(entry.getId()) ? State.Update : State.None);
        }
        for (PrintXEntry printXEntry : orderBookSnapshot.bids.values())
            if (printXEntry.getState() != State.None)
                orderBookSnapshot.actions.add(printXEntry);
        for (PrintXEntry printXEntry : orderBookSnapshot.asks.values())
            if (printXEntry.getState() != State.None)
                orderBookSnapshot.actions.add(printXEntry);

        return orderBookSnapshot;
    }

    public static class OrderBookSnapshotImpl implements OrderBookSnapshot {
        private TreeMap<BigDecimal, XMessageCalculatorImpl.PrintXEntry> bids = new TreeMap<>();
        private TreeMap<BigDecimal, XMessageCalculatorImpl.PrintXEntry> asks = new TreeMap<>();
        private List<XMessageCalculatorImpl.PrintXEntry> actions = new LinkedList<>();

        private Collection<XMessageCalculatorImpl.PrintXEntry> unmodifiableBids = Collections.unmodifiableCollection(bids.descendingMap().values());
        private Collection<XMessageCalculatorImpl.PrintXEntry> unmodifiableAsks = Collections.unmodifiableCollection(asks.values());

        public Collection<XMessageCalculatorImpl.PrintXEntry> getBids() {
            return unmodifiableBids;
        }

        public Collection<XMessageCalculatorImpl.PrintXEntry> getAsks() {
            return unmodifiableAsks;
        }

        public List<XMessageCalculatorImpl.PrintXEntry> getActions() {
            return actions;
        }
    }
}
