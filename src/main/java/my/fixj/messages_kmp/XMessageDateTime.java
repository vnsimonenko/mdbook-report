package my.fixj.messages_kmp;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMessageDateTime {
    private static DecimalFormat df1 = (DecimalFormat) DecimalFormat.getInstance();
    private static DecimalFormat df2 = (DecimalFormat) DecimalFormat.getInstance();
    //yyyyMMdd-HH:mm:ss
    private int year = -1;
    private int month = -1;
    private int day = -1;
    private int hour = -1;
    private int minute = -1;
    private int second = -1;

    public XMessageDateTime() {
        df1.applyPattern("0000");
        df2.applyPattern("00");
    }

    public XMessageDateTime setYear(int year) {
        this.year = year;
        return this;
    }

    public XMessageDateTime setMonth(int month) {
        this.month = month;
        return this;
    }

    public XMessageDateTime setDay(int day) {
        this.day = day;
        return this;
    }

    public XMessageDateTime setHour(int hour) {
        this.hour = hour;
        return this;
    }

    public XMessageDateTime setMinute(int minute) {
        this.minute = minute;
        return this;
    }

    public XMessageDateTime setSecond(int second) {
        this.second = second;
        return this;
    }

    public XMessageDateTime setDate(Date date) {
        if (date == null) return this;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        setYear(c.get(Calendar.YEAR)).setMonth(c.get(Calendar.MONTH) + 1).setDay(c.get(Calendar.DAY_OF_MONTH));
        return this;
    }

    public String toString() {
        String dateTime = "";
        if (year < 0) return null;
        dateTime += df1.format(year);
        if (month < 0) return dateTime;
        dateTime += df2.format(month);
        if (day < 0) return dateTime;
        dateTime += df2.format(day);
        if (hour < 0) return dateTime;
        dateTime += "-" + df2.format(hour);
        if (minute < 0) return dateTime;
        dateTime += ":" + df2.format(minute);
        if (second < 0) return dateTime;
        dateTime += ":" + df2.format(second);
        return dateTime;
    }

    public XMessageDateTime setTime(String text) {
        Matcher matcher = Pattern.compile("([0-9]{0,2})[:]{0,1}([0-9]{0,2})[:]{0,1}([0-9]{0,2})").matcher(text);
        if (matcher.find()) {
            if (matcher.groupCount() >= 1 && !matcher.group(1).isEmpty()) {
                setHour(Integer.parseInt(matcher.group(1)));
            }
            if (matcher.groupCount() >= 2 && !matcher.group(2).isEmpty()) {
                setMinute(Integer.parseInt(matcher.group(2)));
            }
            if (matcher.groupCount() >= 3 && !matcher.group(3).isEmpty()) {
                setSecond(Integer.parseInt(matcher.group(3)));
            }
        }
        return this;
    }
}