package my.fixj.messages_kmp;

import my.fixj.messages.XMessage;

import java.util.Iterator;
import java.util.function.Predicate;

class XMessageContainer {
    private LinkedItem head = new LinkedItem();
    private LinkedItem last;
    private final Object notify = new Object();
    private volatile boolean isCompleted = false;
    private volatile int numberOfXMessages = 0;

    XMessageContainer() {
    }

    Iterator<LinkedItem> iterator() {
        return new XMessageIterator(null);
    }

    Iterator<LinkedItem> iterator(Predicate<XMessage> messagePredicate) {
        return new XMessageIterator(messagePredicate);
    }

    synchronized void clearHead() {
        head = null;
    }

    void complete() {
        if (isCompleted)
            throw new RuntimeException("XMessageContainer is already completed");
        isCompleted = true;
        if (last != null)
            last.next = new LinkedItem();
    }

    void add(String message) {
        synchronized (notify) {
            if (isCompleted)
                throw new RuntimeException("XMessageContainer is filled");
            if (message == null)
                throw new RuntimeException("xMessage is null");

            if (last == null)
                last = head;
            last.next = new LinkedItem(message);
            last = last.next;

            notify.notifyAll();
            numberOfXMessages++;
        }
    }

    int size() {
        return numberOfXMessages;
    }

    class XMessageIterator implements Iterator<LinkedItem> {
        private LinkedItem item;
        private Predicate<XMessage> messagePredicate;

        private XMessageIterator(Predicate<XMessage> messagePredicate) {
            this.messagePredicate = messagePredicate;
        }

        @Override
        public boolean hasNext() {
            throw new UnsupportedOperationException();
        }

        @Override
        public synchronized LinkedItem next() {
            try {
                if (item == null)
                    item = head;

                LinkedItem next = item.next;
                while (next == null || !next.isEmpty() && messagePredicate != null && !messagePredicate.test(next.xMessage)) {
                    synchronized (notify) {
                        notify.wait(100);
                    }
                    next = item.next;
                    if (next != null && next.isTail())
                        break;
                }
                if (next.isEmpty())
                    return null;
                else {
                    item = next;
                    return item;
                }
            } catch (InterruptedException e) {
                return null;
            }
        }
    }

    static class LinkedItem {
        private final String message;
        private XMessage xMessage;
        private LinkedItem next;

        LinkedItem() {
            message = null;
        }

        LinkedItem(String message) {
            this.message = message;
        }

        XMessage getXMessage() {
            return xMessage;
        }

        synchronized String getMessage() {
            return message;
        }

        synchronized void setXMessage(XMessage xMessage) {
            this.xMessage = xMessage;
        }

        boolean isEmpty() {
            return message == null;
        }

        boolean isTail() {
            return xMessage == null && message == null;
        }
    }
}
