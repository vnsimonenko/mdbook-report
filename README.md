# market data book

Прм первом запуске программы для os linux в домашнем каталоге создается каталог        
.market-data-book
```
home
 .market-data-book
    logs
    application.properties
```
application.properties конфигурационный файл.   
Правила формирования отчета изложены в файле [XMessageCalculatorTest.java](https://bitbucket.org/vnsimonenko/mdbook-report/src/master/src/test/java/my/fixj/messages/XMessageCalculatorTest.java)

[Видео mdbook-report.mp4](https://drive.google.com/file/d/1PrF_WNqywxeWivYcF72iVVPzK51V9W7M/view?usp=sharing)

### Requirements and restrictions:
* Its work requires jre 1.8
* Launching from the command line: java -jar target/mdbook-report.jar or mdbook-report.sh
* Отчеты могут быть составными (отдельные страницы)